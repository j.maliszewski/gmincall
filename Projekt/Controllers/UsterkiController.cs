﻿using Projekt.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Projekt.Controllers
{
    public class UsterkiController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult ListaUsterek()
        {
            return View(db.usterki.ToList());
        }
        [Authorize]
        public ActionResult DodajZgloszenie()
        {
            Usterki usterka = new Usterki();
            return View(usterka);
        }
        [Authorize]
        [HttpPost]
        public ActionResult DodajZgloszenie(Usterki usterka)
        {
            string fileName = Path.GetFileNameWithoutExtension(usterka.ImageFile.FileName);
            string extension = Path.GetExtension(usterka.ImageFile.FileName);
            fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
            usterka.ImagePath = "~/Image/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Image/"), fileName);
            usterka.ImageFile.SaveAs(fileName);
            //if (!ModelState.IsValid)
            //{
            //    return View(usterka);
            //}
            //else
            //{
                ViewBag.Message = "Dodano zgłoszenie";
                db.usterki.Add(usterka);
                db.SaveChanges();
                return View();
         //   }
        }
    }
}