﻿using Projekt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Projekt.Controllers
{
    public class UslugiPubliczneController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult ListaUslug()
        {
            return View(db.uslugiPubliczne.ToList());
        }
        [Authorize]
        public ActionResult DodajUsluge()
        {
            UslugiPubliczne usluga = new UslugiPubliczne();
            return View(usluga);
        }
        [Authorize]
        [HttpPost]
        public ActionResult DodajUsluge(UslugiPubliczne usluga)
        {
            if (!ModelState.IsValid)
            {
                return View(usluga);
            }
            else
            {
            ViewBag.Message = "Dodano usługę";
            db.uslugiPubliczne.Add(usluga);
            db.SaveChanges();
            return View();
            }
        }
    }
}