namespace Projekt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nextOne : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UslugiPublicznes",
                c => new
                    {
                        idUslugiPublicznej = c.Int(nullable: false, identity: true),
                        nazwa = c.String(),
                        adres = c.String(),
                        telefon = c.String(),
                        udogodnienia = c.String(),
                    })
                .PrimaryKey(t => t.idUslugiPublicznej);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UslugiPublicznes");
        }
    }
}
