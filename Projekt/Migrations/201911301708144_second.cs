namespace Projekt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class second : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Usterkis", "status", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Usterkis", "status");
        }
    }
}
