namespace Projekt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class first : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Usterkis", "ImagePath", c => c.String());
            AddColumn("dbo.Usterkis", "wspolrzedne", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Usterkis", "wspolrzedne");
            DropColumn("dbo.Usterkis", "ImagePath");
        }
    }
}
